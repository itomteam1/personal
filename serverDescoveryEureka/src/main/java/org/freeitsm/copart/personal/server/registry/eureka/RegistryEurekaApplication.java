package org.freeitsm.copart.personal.server.registry.eureka;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import org.springframework.web.filter.CommonsRequestLoggingFilter;

@SpringBootApplication
@EnableEurekaServer
@EnableEurekaClient
public class RegistryEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistryEurekaApplication.class, args);
	}
	
	// workaround to avoid error in Spring boot 2.0.0.RC1
	// NoSuchBeanDefinitionException: No qualifying bean of type 'javax.servlet.Filter' available
	@Bean
	public Filter webRequestLoggingFilter() {
	    return new CommonsRequestLoggingFilter();
	}
}
