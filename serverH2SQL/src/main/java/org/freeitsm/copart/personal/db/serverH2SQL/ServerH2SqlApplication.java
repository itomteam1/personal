package org.freeitsm.copart.personal.db.serverh2sql;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class ServerH2SqlApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerH2SqlApplication.class);

	@Value("${app.h2.port}")
	private String h2Port;

	public static void main(String[] args) {
		SpringApplication.run(ServerH2SqlApplication.class, args);
	}
	
    /**
     * Start internal H2 server so we can query the DB from IDE
     *
     * @return H2 Server instance
     * @throws SQLException
     */
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2Server() throws SQLException {
    	LOGGER.info("Starting H2 Server on port " + h2Port);
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", h2Port);
    }	
}
