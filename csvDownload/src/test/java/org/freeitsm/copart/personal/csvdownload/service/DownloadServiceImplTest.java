package org.freeitsm.copart.personal.csvdownload.service;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.freeitsm.copart.personal.csvdownload.service.DownloadServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

public class DownloadServiceImplTest {
	
	DownloadServiceImpl downloadService = new DownloadServiceImpl();

	@BeforeClass
	public static void init() {
		
	}
	
	@Test
	public void downloadTest() throws IOException {
		String downloadUrl = "https://raw.githubusercontent.com/sqshq/PiggyMetrics/master/LICENCE";
		String toFile = "./src/main/resources/download/salesdata.csv";
		long bytes = downloadService.download(downloadUrl, toFile);
		
		assertThat(bytes, greaterThan(1100l));  // 1107
	}

}
