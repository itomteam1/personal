package org.freeitsm.copart.personal.csvdownload.controller;

import java.io.IOException;

import org.freeitsm.copart.personal.csvdownload.bean.FileInfo;
import org.freeitsm.copart.personal.csvdownload.service.DownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	DownloadService downloadService;

    @RequestMapping(value = "/download")
    public ResponseEntity<FileInfo> findCities(@RequestBody FileInfo fileInfo) throws IOException {
    	long bytes = downloadService.download(fileInfo.getDownloadUrl(), fileInfo.getToFile());
    	fileInfo.setBytes(bytes);
        return new ResponseEntity<>(fileInfo, HttpStatus.OK);
    }
	
}
