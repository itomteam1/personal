package org.freeitsm.copart.personal.csvdownload.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.springframework.stereotype.Service;

@Service
public class DownloadServiceImpl implements DownloadService {

    public long download(String urlStr, String toFile) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(toFile);
        long bytesTransfered = fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
        return bytesTransfered;
    }
}
