package org.freeitsm.copart.personal.csvdownload.bean;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
/*
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
*/
public class FileInfo {

	private String downloadUrl;
	private String toFile;
	private long bytes;
	
}
