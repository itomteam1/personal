package org.freeitsm.copart.personal.csvdownload.service;

import java.io.IOException;

public interface DownloadService {

    public long download(String urlStr, String toFile) throws IOException;

}