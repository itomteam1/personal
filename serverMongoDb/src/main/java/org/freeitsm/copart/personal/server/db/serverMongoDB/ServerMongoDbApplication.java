package org.freeitsm.copart.personal.server.db.serverMongoDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
@EnableDiscoveryClient
public class ServerMongoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerMongoDbApplication.class, args);
	}
}
