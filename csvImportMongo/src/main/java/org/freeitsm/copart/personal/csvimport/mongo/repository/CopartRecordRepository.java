package org.freeitsm.copart.personal.csvimport.mongo.repository;

import org.freeitsm.copart.personal.csvimport.mongo.bean.CopartRecord;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CopartRecordRepository extends MongoRepository<CopartRecord, Long>{
	
}
