package org.freeitsm.copart.personal.csvimport.mongo.service;

import java.io.IOException;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.mongo.bean.CopartRecord;

public interface CsvService {
	
	public List<CopartRecord> loadAll(String csvFileName) throws IOException;

}
