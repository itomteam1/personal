package org.freeitsm.copart.personal.csvimport.mongo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.mongo.bean.CopartRecord;
import org.freeitsm.copart.personal.csvimport.mongo.service.CsvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@ControllerAdvice
@RefreshScope
public class ControllerMain {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
	
	@Autowired
	CsvService csvService;

	@Value("${import.upload.path}")
    private String uploadFolder;

	@Value("${import.upload.filename}")
	public String fileName;
	


    @Autowired
    private DiscoveryClient discoveryClient;
    
    @GetMapping("/upload")
    public String index() {
        return "upload";
    }
    
    @PostMapping(value = "/file/upload")
    public String fileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
    	
    	LOGGER.info("UPLOADED_FOLDER = {}", uploadFolder);
    	LOGGER.info("CSV_FILENAME = {}", fileName);
    	LOGGER.info("OriginalFilename = {}", file.getOriginalFilename());
    	
    	Path currentDir = Paths.get(".");
    	LOGGER.info("AbsolutePath = {}", currentDir.toAbsolutePath());

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        Path path = Paths.get(uploadFolder + fileName);
        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

            redirectAttributes.addFlashAttribute("message2",
                    "File destination: '" + path.toString() + "'");

        } catch (IOException e) {
            LOGGER.error(e.toString());
            
            redirectAttributes.addFlashAttribute("message",
                    "File upload ERROR '" + file.getOriginalFilename() + "'");

            redirectAttributes.addFlashAttribute("message2",
                    "File destination: '" + path.toString() + "'");

        }

        return "redirect:/uploadStatus";
    }
    
    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

    @RequestMapping(value = "/file/import")
    public List<CopartRecord> fileImport() throws IOException {
        List<CopartRecord> copartRecordList = csvService.loadAll(uploadFolder + fileName);
        return copartRecordList;
    }
    

    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
    
//    @RequestMapping(value = "/import")
//    public List<CopartRecord> findCities() throws IOException {
////    	String csvFileName = "autotest.csv";
////    	String csvFileName = "salesdata.csv";
//        List<CopartRecord> copartRecordList = csvService.loadAll(CSV_FILENAME);
//        return copartRecordList;
//    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {
        String bodyOfResponse = "ERROR";
        return handleExceptionInternal(ex, bodyOfResponse, 
          new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
    
    private ResponseEntity<Object> handleExceptionInternal(Exception ex, String bodyOfResponse
    		, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request){
	    return new ResponseEntity<>(
	    		" " + bodyOfResponse + " =  " + ex.toString(), httpHeaders, httpStatus);
    }

    
}
