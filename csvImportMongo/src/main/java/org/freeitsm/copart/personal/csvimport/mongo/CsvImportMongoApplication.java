package org.freeitsm.copart.personal.csvimport.mongo;

import org.freeitsm.copart.personal.csvimport.mongo.config.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;

@RibbonClients(defaultConfiguration = RibbonConfig.class)
@EnableDiscoveryClient
@SpringBootApplication
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class CsvImportMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvImportMongoApplication.class, args);
	}
}
