package org.freeitsm.copart.personal.service.fileupload.feign;

import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.freeitsm.copart.personal.service.fileupload.domain.ImportStatus;
import org.freeitsm.copart.personal.service.fileupload.domain.ServiceResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("h2client-service")
public interface DbClient {

    @PostMapping(value = "/import")
    public ServiceResponse<ImportStatus> importRecords(@RequestBody ImportRecord importRecord);

}
