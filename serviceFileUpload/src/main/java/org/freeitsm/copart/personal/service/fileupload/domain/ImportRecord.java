package org.freeitsm.copart.personal.service.fileupload.domain;

import lombok.Data;

@Data
public class ImportRecord {
	
	private String uploadFolder;
	private String fileName;

	public ImportRecord(String uploadFolder, String fileName) {
		this.uploadFolder = uploadFolder;
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return uploadFolder + fileName;
	}

}
