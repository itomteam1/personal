package org.freeitsm.copart.personal.service.fileupload.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RefreshScope
@Service
@Validated
public class UploadServiceImpl implements UploadService {

	@Value("${import.upload.path}")
    private String uploadFolder;

	@HystrixCommand(groupKey="UploadService", commandKey = "WriteToDiskService")
//	@HystrixProperty(name = "hystrix.command.default.execution.timeout.enabled", value = "false")
	@Override
	public ImportRecord writeToDisk(MultipartFile file) throws IOException {
    	log.info(" > Save File to disk");
    	log.info("UPLOADED_FOLDER = {}", uploadFolder);
    	log.info("OriginalFilename = {}", file.getOriginalFilename());
    	
    	Path currentDir = Paths.get(".");
    	log.info("AbsolutePath = {}", currentDir.toAbsolutePath());
    	
    	File dir = new File(uploadFolder);
    	
    	if(!dir.isDirectory()) {
    		throw new IOException("Directory " + uploadFolder + " does not exists." );
    	}

        String fileName = FilenameUtils.getName(file.getOriginalFilename());

        log.info("uploadFolder = {}", uploadFolder);
        log.info("fileName = {}", fileName);

		ImportRecord importRecord = new ImportRecord(uploadFolder, fileName);

        
        Path path = null;
        try {
        	path = Paths.get(importRecord.toString());
        }catch(InvalidPathException e) {
        	log.error("Can't get path for '" + uploadFolder + fileName + "'");
        	throw e;
        }
        byte[] bytes = file.getBytes();
        Files.write(path, bytes);

    	log.info(" < End Save File to disk");

        return importRecord;

	}
	
}
