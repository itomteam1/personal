package org.freeitsm.copart.personal.service.fileupload.service;

import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.freeitsm.copart.personal.service.fileupload.domain.ServiceResponse;

public interface ImportNotificationService {

	ServiceResponse notifyImport(ImportRecord importRecord);
}
