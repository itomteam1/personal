package org.freeitsm.copart.personal.service.fileupload.domain;

import lombok.Data;

@Data
public class ServiceResponse<T> {

	private Integer errorCode = 0;
	private String erorrMessage = "";
	
	private T responseBody;
}
