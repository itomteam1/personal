package org.freeitsm.copart.personal.service.fileupload.service;

import java.io.IOException;

import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {

	public ImportRecord writeToDisk(MultipartFile file) throws IOException;
	
}
