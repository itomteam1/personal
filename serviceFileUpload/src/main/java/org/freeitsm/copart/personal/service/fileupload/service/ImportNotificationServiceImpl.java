package org.freeitsm.copart.personal.service.fileupload.service;

import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.freeitsm.copart.personal.service.fileupload.domain.ImportStatus;
import org.freeitsm.copart.personal.service.fileupload.domain.ServiceResponse;
import org.freeitsm.copart.personal.service.fileupload.feign.DbClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class ImportNotificationServiceImpl implements ImportNotificationService {
	
	@Autowired
	DbClient dbClient;

	@Override
	public ServiceResponse<ImportStatus> notifyImport(ImportRecord importRecord) {
		log.info("Send feign POST to h2client-service/import");
		return dbClient.importRecords(importRecord);
	}

}
