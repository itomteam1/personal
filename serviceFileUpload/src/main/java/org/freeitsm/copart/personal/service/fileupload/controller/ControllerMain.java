package org.freeitsm.copart.personal.service.fileupload.controller;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;

import org.freeitsm.copart.personal.service.fileupload.domain.ImportRecord;
import org.freeitsm.copart.personal.service.fileupload.domain.ImportStatus;
import org.freeitsm.copart.personal.service.fileupload.domain.ImportStatusEnum;
import org.freeitsm.copart.personal.service.fileupload.domain.ServiceResponse;
import org.freeitsm.copart.personal.service.fileupload.service.ImportNotificationService;
import org.freeitsm.copart.personal.service.fileupload.service.UploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

@Controller
@ControllerAdvice
@RefreshScope
@Slf4j
public class ControllerMain {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
	
	@Autowired
	ImportNotificationService importNotificationService;

	@Autowired
	UploadService uploadService;

    @Autowired
    private DiscoveryClient discoveryClient;
    
	@HystrixCommand(groupKey="Controller", commandKey = "IndexController")
    @GetMapping("/index")
    public String index() {
        return "upload";
    }

	@HystrixCommand(groupKey="Controller", commandKey = "FileUploadController")
//	@HystrixProperty(name = "hystrix.command.default.execution.timeout.enabled", value = "false") // https://stackoverflow.com/questions/27375557/hystrix-command-fails-with-timed-out-and-no-fallback-available
	@PostMapping(value = "/file")
    public String fileUpload(@RequestParam("file") MultipartFile file
    		, @RequestParam(value = "notify", defaultValue = "false") boolean notify
    		, Model model) {
		
		log.info("Request: /file");
		
		ImportStatus importStatus = new ImportStatus();
        importStatus.setStatus(ImportStatusEnum.UPLOAD_START);
   	
        if (file.isEmpty()) {
        	model.addAttribute("message", "Please select a file to upload");
        	model.addAttribute("messageImport", "");
            importStatus.setStatus(ImportStatusEnum.UPLOAD_ERR_NOFILE);
            return "uploadStatus";
        }

        model.addAttribute("message", "Upload: Started.");
    	model.addAttribute("messageImport", "");
        
        ImportRecord importRecord = null;
        importStatus.setStatus(ImportStatusEnum.UPLOAD_SAVE_TO_DISK_START);
        
        try {
        	importRecord = uploadService.writeToDisk(file);

            importStatus.setStatus(ImportStatusEnum.UPLOAD_SAVE_TO_DISK_OK);

        	model.addAttribute("message",
                    "Upload: SUCCESS. '" + file.getOriginalFilename() + "' to '" + importRecord.toString() + "'");
        	if(notify) {
                importStatus.setStatus(ImportStatusEnum.UPLOAD_NOTIFY_IMPORT);
            	model.addAttribute("messageImport", "Import: Notify.");
        		ServiceResponse<ImportStatus> importResponse = importNotificationService.notifyImport(importRecord);
        		if(importResponse.getErrorCode() != 0) {
                	model.addAttribute("messageImport", "Import: ERROR. " + importResponse.getErorrMessage());
        		}else {
        			importStatus = importResponse.getResponseBody();
                	model.addAttribute("messageImport",
                            "Import: Started. Records to proceed: " + importStatus.getRecordsTotal());
        		}
        	} else {
            	model.addAttribute("messageImport", "Import: NOT REQUESTED. Records in file: " + importStatus.getRecordsTotal());
        	}

        } catch (IOException e) {
	        LOGGER.error(e.toString());
	        importStatus.setStatus(ImportStatusEnum.UPLOAD_ERR_IO);
	        
	        model.addAttribute("message",
	                "File upload IO ERROR '" + e.toString() + "' Status: " + importStatus.toString() );
	        return "uploadStatus";
	    } catch (UndeclaredThrowableException e) {
	        importStatus.setStatus(ImportStatusEnum.UPLOAD_ERR_UNDECLARED);
	        model.addAttribute("message",
	                "File upload Undeclared ERROR '" + e.getCause().toString() + "' Status: " + importStatus.toString());
	        return "uploadStatus";
	    } catch (Exception e) {
	        LOGGER.error(e.toString());
	        importStatus.setStatus(ImportStatusEnum.UPLOAD_ERR_EXCEPTION);
	        model.addAttribute("message",
	                "File upload ERROR '" + e.toString() + "' Status: " + importStatus.toString());
	        return "uploadStatus";
	    }

        return "uploadStatus";
    }
    
    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
    
    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {
        String bodyOfResponse = "ERROR";
        return handleExceptionInternal(ex, bodyOfResponse, 
          new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
    
    private ResponseEntity<Object> handleExceptionInternal(Exception ex, String bodyOfResponse
    		, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request){
	    return new ResponseEntity<>(
	    		" " + bodyOfResponse + " =  " + ex.toString(), httpHeaders, httpStatus);
    }
    
}
