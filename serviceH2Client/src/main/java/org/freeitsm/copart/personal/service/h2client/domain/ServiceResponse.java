package org.freeitsm.copart.personal.service.h2client.domain;

import lombok.Data;

@Data
public class ServiceResponse<T> {

	private Integer errorCode = 0;
	private String erorrMessage = "";
	
	private T responseBody;
}
