package org.freeitsm.copart.personal.service.h2client.controller;

import org.freeitsm.copart.personal.service.h2client.domain.ImportRecord;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatus;
import org.freeitsm.copart.personal.service.h2client.domain.ServiceResponse;
import org.freeitsm.copart.personal.service.h2client.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Controller
public class ControllerDb {

    @Autowired
    private ApplicationContext appContext;

	@Autowired
	ImportService importService;

	@HystrixCommand(groupKey="Controller", commandKey = "ImportController")
    @PostMapping(value = "/import")
    @ResponseBody
    public ServiceResponse<ImportStatus> importRecords(@RequestBody ImportRecord importRecord){
        ServiceResponse<ImportStatus> serviceResponse = new ServiceResponse<>();
        try {
        	ImportStatus importStatus = importService.importAllData(importRecord);
            serviceResponse.setResponseBody(importStatus);
        }catch(Exception e) {
        	serviceResponse.setErrorCode(1);
        	serviceResponse.setErorrMessage(e.toString());
        }finally {
        	
        }
        return serviceResponse;
    }

	@HystrixCommand(groupKey="Controller", commandKey = "StatusController")
    @GetMapping(value = "/status.json")
    @ResponseBody
    public ImportStatus importStatusJson(){
        return importService.getImportStatus();
    }

    @GetMapping(value = "/status")
    public String importStatus(Model model){
    	ImportStatus importStatus = importService.getImportStatus();
     	model.addAttribute("message", "Imported " + importStatus.getRecordsProcessed() + " of " + importStatus.getRecordsTotal());
     	model.addAttribute("messageStatus", "Current Status: " + importStatus.getStatus());
        return "importStatus";
    }

    @GetMapping(value = "/stop")
    public void stop(){
        SpringApplication.exit(appContext, () -> 0);
    }

}
