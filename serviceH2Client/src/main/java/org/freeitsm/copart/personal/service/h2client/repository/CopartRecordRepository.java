package org.freeitsm.copart.personal.service.h2client.repository;

import org.freeitsm.copart.personal.service.h2client.domain.CopartRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CopartRecordRepository  extends CrudRepository<CopartRecord, Long>{
	
}
