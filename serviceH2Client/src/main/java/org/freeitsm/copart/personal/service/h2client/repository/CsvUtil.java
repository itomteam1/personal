package org.freeitsm.copart.personal.service.h2client.repository;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.freeitsm.copart.personal.service.h2client.domain.CopartRecord;
import org.springframework.stereotype.Component;

import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;
import com.univocity.parsers.csv.UnescapedQuoteHandling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CsvUtil {

	public static void printAbsolutePath() {
		   File directory = new File("./");
		   System.out.println("Absolute path = " + directory.getAbsolutePath());
	}

	public File getFileByName(String fileName) {
		printAbsolutePath();
		return new File(fileName);
	}
	
	public List<CopartRecord> loadAllRecordsFromFileToList(File csvFile){
		
		log.info(" > Start load data from file to List");
		
		List<CopartRecord> list = new LinkedList<>();
	
	    CsvParserSettings parserSettings = new CsvParserSettings();
	    parserSettings.getFormat().setLineSeparator("\n");
	    parserSettings.setUnescapedQuoteHandling(UnescapedQuoteHandling.STOP_AT_CLOSING_QUOTE);
	
	    CsvRoutines routines = new CsvRoutines(parserSettings); 
	    for (CopartRecord object : routines.iterate(CopartRecord.class, csvFile, "UTF-8")) {
//			System.out.println("CopartRecord = " + object.toString());
	        list.add(object);
	    }
		log.info(" < End load data from file to List");
		return list;
	}
	
}
