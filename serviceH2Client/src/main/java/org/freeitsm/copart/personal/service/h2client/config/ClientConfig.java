package org.freeitsm.copart.personal.service.h2client.config;

import org.freeitsm.copart.personal.service.h2client.domain.ImportStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

	@Bean
	public ImportStatus importStatus() {
		return new ImportStatus();
	}
}
