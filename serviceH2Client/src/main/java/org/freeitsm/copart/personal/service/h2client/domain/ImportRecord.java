package org.freeitsm.copart.personal.service.h2client.domain;

import lombok.Data;

@Data
public class ImportRecord {
	
	private String uploadFolder;
	private String fileName;
	
	@Override
	public String toString() {
		return uploadFolder + fileName;
	}

}
