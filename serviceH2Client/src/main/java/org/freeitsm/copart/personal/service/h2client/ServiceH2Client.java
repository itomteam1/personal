package org.freeitsm.copart.personal.service.h2client;

import org.freeitsm.copart.personal.service.h2client.repository.CsvUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableHystrix
@EnableDiscoveryClient
@EnableAsync
public class ServiceH2Client {

	public static void main(String[] args) {
		SpringApplication.run(ServiceH2Client.class, args);
		CsvUtil.printAbsolutePath();
	}
}
