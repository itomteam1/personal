package org.freeitsm.copart.personal.service.h2client.service;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;

import org.freeitsm.copart.personal.service.h2client.domain.CopartRecord;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatus;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatusEnum;
import org.freeitsm.copart.personal.service.h2client.repository.CopartRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ImportTask {

	@Autowired
	CopartRecordRepository copartRecordRepository;

	@Value("${app.import.pageSize}")
	Integer pageSize;

	ImportStatus importStatus;

	@Async
	public Future<ImportStatus> importToDb(List<CopartRecord> copartRecordList, ImportStatus importStatus) {
		this.importStatus = importStatus;
		try {
			deleteOldRecordsFromDb();
			saveNewRecordsToDb(copartRecordList, pageSize);
			importStatus.setStatus(ImportStatusEnum.IMPORT_OK);
		} catch(Exception e) {
			log.error(e.getMessage());
			importStatus.setStatus(ImportStatusEnum.IMPORT_ERROR);
		}
		return new AsyncResult<>(importStatus);
	}

	private void deleteOldRecordsFromDb() {
		importStatus.setStatus(ImportStatusEnum.IMPORT_DELETE_OLD_START);
		log.info(" > Delete old records...");
		copartRecordRepository.deleteAll();
		log.info(" < End Delete old records");
		importStatus.setStatus(ImportStatusEnum.IMPORT_DELETE_OLD_OK);
	}

	public void saveNewRecordsToDb(List<CopartRecord> copartRecordList, int pageSize) {
		log.info(" > Save List to DB");
		for(int i = 1; i<= copartRecordList.size()/pageSize + 1; i++) {
			List<CopartRecord> tmpList = getPage(copartRecordList, i, pageSize);
			copartRecordRepository.save(tmpList);
			importStatus.addRecordsProcessed(tmpList.size());
			log.debug("Records Processed: " + importStatus.getRecordsProcessed());
		}
		log.info(" < End Save List to DB. {} of {} records.", importStatus.getRecordsProcessed(), importStatus.getRecordsTotal());
	}

	/*
	 * returns a view (not a new list) of the sourceList for the 
	 * range based on page and pageSize
	 * @param sourceList
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {
	    if(pageSize <= 0 || page <= 0) {
	        throw new IllegalArgumentException("invalid page size: " + pageSize);
	    }

	    int fromIndex = (page - 1) * pageSize;
	    if(sourceList == null || sourceList.size() < fromIndex){
	        return Collections.emptyList();
	    }

	    // toIndex exclusive
	    return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
	}
	
}
