package org.freeitsm.copart.personal.service.h2client.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.freeitsm.copart.personal.service.h2client.domain.CopartRecord;
import org.freeitsm.copart.personal.service.h2client.domain.ImportRecord;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatus;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatusEnum;
import org.freeitsm.copart.personal.service.h2client.repository.CopartRecordRepository;
import org.freeitsm.copart.personal.service.h2client.repository.CsvUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImportServiceImpl implements ImportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportServiceImpl.class);

	@Autowired
	CopartRecordRepository copartRecordRepository;
	@Autowired
	CsvUtil csvUtil;
	
	@Autowired
	ImportStatus importStatus;
	
	@Autowired
	ImportTask importTask;

//	@HystrixCommand(groupKey="ImportService", commandKey = "ImportAllDataService")
//	@HystrixProperty(name = "hystrix.command.default.execution.timeout.enabled", value = "false")
	@Override
	public ImportStatus importAllData(ImportRecord importRecord) throws IOException, InterruptedException, ExecutionException {
		importStatus.setStatus(ImportStatusEnum.IMPORT_START);
		importStatus.setRecordsProcessed(0);
    	File csvFile = csvUtil.getFileByName(importRecord.toString());
		importStatus.setStatus(ImportStatusEnum.IMPORT_GETFILE_FROM_DISK_OK);
    	LOGGER.info("Start import");
		List<CopartRecord> copartRecordList = csvUtil.loadAllRecordsFromFileToList(csvFile);
		importStatus.setStatus(ImportStatusEnum.IMPORT_FILE_TO_LIST_OK);
		importStatus.setRecordsTotal(copartRecordList.size());
		
		importTask.importToDb(copartRecordList, importStatus);
    	LOGGER.info("Import started. Return status.");
		return importStatus;
	}
	
	@Override
	public ImportStatus getImportStatus() {
		return importStatus;
	}
	


}
