package org.freeitsm.copart.personal.service.h2client.service;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.freeitsm.copart.personal.service.h2client.domain.ImportRecord;
import org.freeitsm.copart.personal.service.h2client.domain.ImportStatus;

public interface ImportService {
	
	ImportStatus importAllData(ImportRecord importRecord) throws IOException, InterruptedException, ExecutionException;

	ImportStatus getImportStatus();

}
