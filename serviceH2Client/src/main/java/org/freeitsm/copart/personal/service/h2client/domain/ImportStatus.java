package org.freeitsm.copart.personal.service.h2client.domain;

import lombok.Data;

@Data
public class ImportStatus {

	Integer recordsProcessed = 0;
	Integer recordsTotal = 0;
	ImportStatusEnum status = ImportStatusEnum.NOT_STARTED;
 
	public void addRecordsProcessed(int records) {
		recordsProcessed += records;
		
	}

}
