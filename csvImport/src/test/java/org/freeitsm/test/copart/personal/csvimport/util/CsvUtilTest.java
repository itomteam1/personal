package org.freeitsm.test.copart.personal.csvimport.util;

//import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;
import org.freeitsm.copart.personal.csvimport.util.CsvUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;
//import org.junit.Assert;

public class CsvUtilTest {
	
	File csvFile = new File("./src/test/resources/download/test.csv");
	
	@Before
	public void init() {
		CsvUtil.printAbsolutePath();
	}
	
	@Test
	public void loadAllTest() throws IOException {
		CsvUtil csvUtil = new CsvUtil();
		List<CopartRecord> list = csvUtil.loadAll(csvFile);
		
		assertThat(list.size(), is(8));
		assertThat(list.get(0).getSpecialNote(), equalTo("TITLE ODOMETER IS \"EXEMPT\"  "));
//		fail("Not yet implemented");
	}

}
