package org.freeitsm.copart.personal.csvimport.controller;

import java.io.IOException;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;
import org.freeitsm.copart.personal.csvimport.service.CsvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	CsvService csvService;

	@Value("./download/salesdata.csv")
	public String csvFileName;

    @RequestMapping(value = "/load")
    public List<CopartRecord> findCities() throws IOException {
//    	String csvFileName = "autotest.csv";
//    	String csvFileName = "salesdata.csv";
        List<CopartRecord> copartRecordList = csvService.loadAll(csvFileName);
        return copartRecordList;
    }

//    @RequestMapping(value = "/load", produces = "text/csv")
//    public void findCities(HttpServletResponse response) throws IOException {
//        List<CopartRecord> cities = (List<CopartRecord>) csvService.loadAll();
//        WriteCsvToResponse.writeCities(response.getWriter(), cities);
//    }
    
}
