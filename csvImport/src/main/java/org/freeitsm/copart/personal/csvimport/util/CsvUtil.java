package org.freeitsm.copart.personal.csvimport.util;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;
import org.springframework.stereotype.Component;

import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;
import com.univocity.parsers.csv.UnescapedQuoteHandling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CsvUtil {

	public static void printAbsolutePath() {
		   File directory = new File("./");
		   System.out.println("Absolute path = " + directory.getAbsolutePath());
	}

	public File getFileByName(String fileName) {
		printAbsolutePath();
		return new File(fileName);
	}
	
//	public List<CopartRecord> loadAll(File csvFile) throws IOException{
//		
//		log.debug("Start CSV parsing");
////		CsvMapper csvMapper = new CsvMapper();    
//		CsvMapper csvMapper = new CsvMapper().enable(CsvParser.Feature.SKIP_EMPTY_LINES);    
//	    CsvSchema schema = csvMapper.typedSchemaFor(CopartRecord.class).withHeader();
////		MappingIterator<CopartRecord> dataIterator = csvMapper.readerFor(CopartRecord.class).with(schema)
////				.readValues(csvFile);
//		
//		MappingIterator<CopartRecord> it = csvMapper.readerFor(CopartRecord.class).with(schema).readValues(csvFile);
//		while (it.hasNext()){
//			CopartRecord row = it.next();
//			System.out.println("CopartRecord = " + row.toString());
//		}
//
////		MappingIterator<CopartRecord> personIter = new CsvMapper().readerWithTypedSchemaFor(CopartRecord.class).readValues(csvFile);
//		return it.readAll();
//	}
	
	public List<CopartRecord> loadAll(File csvFile){
		
		List<CopartRecord> list = new LinkedList<>();
	
	    CsvParserSettings parserSettings = new CsvParserSettings();
	    parserSettings.getFormat().setLineSeparator("\n");
	    parserSettings.setUnescapedQuoteHandling(UnescapedQuoteHandling.STOP_AT_CLOSING_QUOTE);
	
	    CsvRoutines routines = new CsvRoutines(parserSettings); 
	    for (CopartRecord object : routines.iterate(CopartRecord.class, csvFile, "UTF-8")) {
//			System.out.println("CopartRecord = " + object.toString());
	        list.add(object);
	    }
		return list;
	}
	
}
