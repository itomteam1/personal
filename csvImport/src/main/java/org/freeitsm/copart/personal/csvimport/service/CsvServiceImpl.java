package org.freeitsm.copart.personal.csvimport.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;
import org.freeitsm.copart.personal.csvimport.repository.CopartRecordRepository;
import org.freeitsm.copart.personal.csvimport.util.CsvUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CsvServiceImpl implements CsvService {
	
	@Autowired
	CsvUtil csvUtil;
	
	@Autowired
	CopartRecordRepository copartRecordRepository;

	@Override
	public List<CopartRecord> loadAll(String csvFileName) throws IOException {
    	File csvFile = csvUtil.getFileByName(csvFileName);
    	log.info("Start import");
		List<CopartRecord> copartRecordList = csvUtil.loadAll(csvFile);
    	log.debug("Start delete");
		copartRecordRepository.deleteAll();
    	log.debug("Start save to DB");
		copartRecordRepository.saveAll(copartRecordList);
		return copartRecordList;
	}

}
