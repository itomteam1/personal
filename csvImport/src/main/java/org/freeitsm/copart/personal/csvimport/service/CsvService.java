package org.freeitsm.copart.personal.csvimport.service;

import java.io.IOException;
import java.util.List;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;

public interface CsvService {
	
	public List<CopartRecord> loadAll(String csvFileName) throws IOException;

}
