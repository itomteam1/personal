package org.freeitsm.copart.personal.csvimport.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.univocity.parsers.annotations.Parsed;

import lombok.Data;

@Entity
@Data
//@JsonPropertyOrder({"yardNumber", "yardName", "saleDate", "dayOfWeek", "saleTime", "timeZone"
//	, "itemNum", "lotNumber", "vehicleType", "year", "make", "modelGroup"
//	, "modelDetail", "bodyStyle", "color", "damageDescription", "secondaryDamage"
//	, "saleTitleState", "saleTitleType", "hasKeys", "lotCondCode", "vin", "odometer"
//	, "odometerBrand", "estRetailValue", "repairCost", "engine", "drive"
//	, "transmission", "fuelType", "cylinders", "runsDrives", "saleStatus", "highBid"
//	, "specialNote", "locationCity", "locationState", "locationZIP"
//	, "locationCountry", "currencyCode", "imageThumbnail", "createDateTime"
//	, "gridRow", "makeAnOfferEligible", "buyItNowPrice", "imageURL"
//})
public class CopartRecord {

/*	
	"Yard number","Yard name","Sale Date M/D/CY","Day of Week","Sale time (HHMM)","Time Zone"
	,"Item#","Lot number","Vehicle Type","Year","Make","Model Group","Model Detail","Body Style","Color"
	,"Damage Description","Secondary Damage","Sale Title State","Sale Title Type","Has Keys-Yes or No","Lot Cond. Code"
	,"VIN","Odometer","Odometer Brand","Est. Retail Value","Repair cost","Engine","Drive","Transmission","Fuel Type","Cylinders"
	,"Runs/Drives","Sale Status","High Bid =non-vix,Sealed=Vix","Special Note"
	,"Location city","Location state","Location ZIP","Location country","Currency Code"
	,"Image Thumbnail","Create Date/Time","Grid/Row","Make-an-Offer Eligible","Buy-It-Now Price","Image URL"
*/	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCopart;

	@Parsed(field = "Yard number")			
	private Integer yardNumber; 		// 1
	@Parsed(field = "Yard name") 
	private String 	yardName; 			// "CA - VALLEJO"
	@Parsed(field = "Sale Date M/D/CY") 
	private Integer	saleDate; 			// 20180105
	@Parsed(field = "Day of Week") 
	private String	dayOfWeek; 			// "FRIDAY"
	@Parsed(field = "Sale time (HHMM)") 
	private Integer	saleTime; 			// 1200
	@Parsed(field = "Time Zone") 
	private String 	timeZone; 			// "PST"
	@Parsed(field = "Item#") 
	private Integer itemNum; 			// 0
	@Parsed(field = "Lot number") 
	private Integer lotNumber; 			//38968717
	@Parsed(field = "Vehicle Type") 
	private String 	vehicleType; 		// "V"
	@Parsed(field = "Year") 
	private Integer year; 				// 2014
	@Parsed(field = "Make") 
	private String 	make; 				// "NISSAN"
	@Parsed(field = "Model Group") 
	private String 	modelGroup; 		// "ALTIMA"
	@Parsed(field = "Model Detail") 
	private String 	modelDetail; 		// "ALTIMA 2.5"
	@Parsed(field = "Body Style") 
	private String 	bodyStyle; 			// "SEDAN 4D"
	@Parsed(field = "Color") 
	private String 	color; 				// "BROWN"
	@Parsed(field = "Damage Description") 
	private String 	damageDescription; 	// "FRONT END"
	@Parsed(field = "Secondary Damage") 
	private String 	secondaryDamage; 	// "MINOR DENT/SCRATCH"
	@Parsed(field = "Sale Title State") 
	private String 	saleTitleState; 	// "CA"
	@Parsed(field = "Sale Title Type") 
	private String 	saleTitleType; 		// "SC"
	@Parsed(field = "Has Keys-Yes or No") 
	private String 	hasKeys; 			// "YES"
	@Parsed(field = "Lot Cond. Code") 
	private String 	lotCondCode; 		// "S"
	@Parsed(field = "VIN") 
	private String 	vin; 				// "1N4AL3AP3EN246226" 
	@Parsed(field = "Odometer") 
	private Float  	odometer; 			// 52746.0
	@Parsed(field = "Odometer Brand") 
	private String 	odometerBrand; 		// "A"
	@Parsed(field = "Est. Retail Value") 
	private Float  	estRetailValue; 	// 12579.0
	@Parsed(field = "Repair cost") 
	private Float  	repairCost; 		// 17070.53
	@Parsed(field = "Engine") 
	private String 	engine; 			// "2.5L  4"
	@Parsed(field = "Drive") 
	private String 	drive; 				// "Front-wheel Drive"
	@Parsed(field = "Transmission") 
	private String 	transmission; 		// "Automatic"
	@Parsed(field = "Fuel Type") 
	private String 	fuelType; 			// "GAS"
	@Parsed(field = "Cylinders") 
	private String 	cylinders; 			// "4"
	@Parsed(field = "Runs/Drives") 
	private String 	runsDrives; 		// "Vehicle Starts"
	@Parsed(field = "Sale Status") 
	private String 	saleStatus; 		// "On Minimum Bid"
	@Parsed(field = "High Bid =non-vix,Sealed=Vix") 
	private String 	highBid; 			// "0.0"
	@Parsed(field = "Special Note") 
	@Column(name="specialNote", columnDefinition="TEXT")
	private String 	specialNote; 		// ""
	@Parsed(field = "Location city") 
	private String 	locationCity; 		// "VALLEJO"
	@Parsed(field = "Location state") 
	private String 	locationState; 		// "CA"
	@Parsed(field = "Location ZIP") 
	private String 	locationZIP; 		// "94590"
	@Parsed(field = "Location country") 
	private String 	locationCountry; 	// "USA"
	@Parsed(field = "Currency Code") 
	private String 	currencyCode; 		// "USD"
	@Parsed(field = "Image Thumbnail") 
	private String 	imageThumbnail; 	// "cs.copart.com/v1/AUTH_svc.pdoc00001/PIX87/ccdea6aa-9766-4d35-bda4-14356bdd3dec.JPG"
	@Parsed(field = "Create Date/Time") 
	private String 	createDateTime; 	// "2017-12-29-04.12.40.000297"
	@Parsed(field = "Grid/Row") 
	private String 	gridRow; 			// "SE006"
	@Parsed(field = "Make-an-Offer Eligible") 
	private String 	makeAnOfferEligible; // "N"
	@Parsed(field = "Buy-It-Now Price") 
	private Float  	buyItNowPrice; 		// 0.0
	@Parsed(field = "Image URL") 
	private String 	imageURL; 			// "http://inventory.copart.io/v1/lotImages/38968717?country=us&brand=cprt&yardNumber=1"
	
	
	

}
