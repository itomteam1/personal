package org.freeitsm.copart.personal.csvimport.repository;

import org.freeitsm.copart.personal.csvimport.bean.CopartRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CopartRecordRepository  extends CrudRepository<CopartRecord, Long>{
	
}
