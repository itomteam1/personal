package org.freeitsm.copart.personal.server.gateway.zuul.config;

import org.freeitsm.copart.personal.server.gateway.zuul.utils.GenericFallbackProvider;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfig {

	@Bean
	public FallbackProvider uploadFallbackProvider() {
		GenericFallbackProvider uploadFallback = new GenericFallbackProvider();
		uploadFallback.setRoute("upload-service");
		return uploadFallback;
	}

}
